EE ?= emacs -Q --batch --eval "(require 'ob-tangle)"

linux: alacritty rofi fcitx5 picom

mac: kitty

kitty: kittyrc kittyday kittynight

day:
	@kitty +kitten themes --reload-in=all "Tokyo Night Day"

night:
	@kitty +kitten themes --reload-in=all "Tokyo Night"

xmonad: xmonad.org
	@ [[ -d ~/.config/xmonad ]] || mkdir -p ~/.config/xmonad
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'
	@ chmod +x ~/.config/xmonad/trayer-padding-icon.sh

xmobar: xmobar.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

kittyrc: kitty.org
	@ [[ -d ~/.config/kitty ]] || mkdir -p ~/.config/kitty/themes
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

kittyday: kittyday.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

kittynight: kittynight.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

kittytoday:
	@kitty +kitten themes --reload-in=all "Tokyo Night Day"

alacritty: alacritty.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

picom: picom.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

rofi: rofi.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

xsettingsd: xsettingsd.org
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

dunst: dunst.org
	@ [[ -d ~/.config/dunst ]] || mkdir -p ~/.config/dunst
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'

fcitx5:
	@ [[ -d ~/.local/share/fcitx5/themes ]] || mkdir -p ~/.local/share/fcitx5/themes/
	@ [[ -d ./fcitx5-nord ]] || git clone https://github.com/tonyfettes/fcitx5-nord.git

aria2: aria2.org
	@ [[ -d ~/.config/aria2 ]] || mkdir -p ~/.config/aria2
	$(EE) --eval '(org-babel-tangle-publish t "$<" "$(@D)/")'
	@ [[ -d ~/Downloads ]] || mkdir -p ~/Downloads
	@ [[ -f ~/Downloads/aria2.session ]] || touch ~/Downloads/aria2.session

clean:
	@ rm -rf ~/.config/xmonad
	@ rm -rf ~/.xmobarrc
	@ rm -rf ~/.config/kitty
	@ rm -rf ~/.alacritty.yml
	@ rm -rf ~/.rofi.rasi
	@ rm -rf ~/.xsettingsd
	@ rm -rf ~/.config/dunst
	@ rm -rf ~/.config/aria2
